
#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "UIImage+UIImageExt.h"
@class DetailViewController;

@interface MasterViewController : UITableViewController<UISearchDisplayDelegate,ABNewPersonViewControllerDelegate,ABPersonViewControllerDelegate>
{
    @private
    NSMutableDictionary *pool;
    dispatch_queue_t globalQ;

    NSMutableDictionary *sectionDic;
    NSMutableDictionary *phoneDic;
    NSMutableDictionary *contactDic;
    NSMutableArray *filteredArray;
    //NSMutableArray *contactNames;
}
@property (strong, nonatomic) DetailViewController *detailViewController;
-(void)loadContacts;
@end
