
#import <UIKit/UIKit.h>

@interface GroupViewController : UITableViewController
{
    NSMutableArray *_groups;
    NSMutableDictionary *_persons;
    NSMutableDictionary *addpersons; //选择的联系人
    BOOL *flag;

}
@property (strong, nonatomic) NSMutableArray       *groups;
@property (strong, nonatomic) NSMutableDictionary  *persons;
@end
