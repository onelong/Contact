//
//  UIImage+UIImageExt.h
//  QRCode
//
//  Created by 黄 隆 on 13-11-1.
//  Copyright (c) 2013年 Onelong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImageExt)

-(UIImage *)scaleBySize:(CGSize)targetSize;
-(UIImage *)scaleToSize:(CGSize)targetSize;

-(UIImage *)scalingAndCroppingForSize:(CGSize)targetSize;

@end
